<?php

namespace App\Http\Controllers;

use App\Http\Resources\MoodResource;
use App\Mood;
use Illuminate\Http\Request;

class MoodController extends Controller
{

    public function __construct(){
        $this->middleware('auth:sanctum');
    }

    public function index(){
        $moods = Mood::paginate(20);
        return MoodResource::collection($moods);
    }

    public function store(Request $request){
        $this->validate($request, [
            'user_id' => ['required'],
            'mood' => ['required', 'integer'],
            'highlight' => ['required', 'string']
        ]);

        $mood = Mood::create([
            'user_id' => $request->input('user_id'),
            'mood' => $request->input('mood'),
            'highlight' => $request->input('highlight')
        ]);

        return new MoodResource($mood);
    }
}
