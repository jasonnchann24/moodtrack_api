<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mood extends Model
{
    protected $table = 'moods';

    protected $fillable = [
        'user_id', 'mood', 'highlight',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
